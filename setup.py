from setuptools import setup, find_packages

setup(
    name='dashboard-facebook',
    version='0.2',
    description='Meltano reports and dashboards for Facebook Ads data',
    packages=find_packages(),
    package_data={'reports': ['*.m5o'], 'dashboards': ['*.m5o'], 'snapshots': ['**/*.m5o']},
    install_requires=[],
)
